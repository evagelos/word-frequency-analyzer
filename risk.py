#!/usr/bin/python3

import re


class WordFrequency:

    def __init__(self, word, frequency):
        self.word = word
        self.frequency = frequency


class WordFrequencyAnalyzer:

    def _frequencies_per_word(self, text):
        """Return a dict with words found in text along with their frequency."""
        frequencies = {}
        text = text.strip()
        words = [w.lower() for w in re.split(r"[^a-zA-Z]+", text) if w != '']

        for word in words:
            try:
                frequencies[word] += 1
            except KeyError:
                frequencies[word] = 1

        return frequencies

    def calculate_highest_frequency(self, text):
        """Return the highest frequency of a word found in text."""
        frequencies = self._frequencies_per_word(text)
        return max(frequencies.values() or [0])

    def calculate_frequency_for_word(self, text, word):
        """Return how many times a word is found in text."""
        frequencies = self._frequencies_per_word(text)
        return frequencies.get(word, 0)

    def calculate_most_frequent_n_words(self, text, n):
        """
        Return a list of the first n words with the highest frequency.

        Order is determined by higher frequency and then alphabetically.
        """
        frequencies = self._frequencies_per_word(text)
        sorted_frequencies = sorted(
            frequencies.items(),
            key=lambda item: (-item[1], item[0]),
        )

        return sorted_frequencies[:n]
