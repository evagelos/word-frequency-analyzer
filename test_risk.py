import pytest

from risk import WordFrequencyAnalyzer

ANALYZER = WordFrequencyAnalyzer()


@pytest.mark.parametrize("text,words", [
    ("", 0),
    (" ", 0),
    ("123", 0),
    ("123foo", 1),
    ("\u2714foo", 1),
    ("\u2714\tfoo", 1),
    ("\u2714\tfoo\r", 1),
    ("foo*\u2714*", 1),
    ("foo FOO Foo fOo foO FOo fOO FoO", 1),
    ("foofoo", 1),
    ("foo^foo", 1),
    ("foo^foo_foo", 1),
    ("foo(foofoo", 2),
    ("\n\n\t", 0),
    ("\nfoo\t", 1),
    ("!@#$% ", 0),
])
def test_how_many_words_exist_in_text(text, words):
    """Ensure searching/splitting words from text works as expected."""
    frequencies = ANALYZER._frequencies_per_word(text)
    assert len(frequencies) == words


@pytest.mark.parametrize("text,frequency", [
    ("", 0),
    ("foo", 1),
    ("foo foo", 2),
    ("foo bar", 1),
    ("foo foo bar", 2),
    ("foo foo bar bar bar", 3),
    ("foo FOO Foo fOo foO FOo fOO FoO", 8),
])
def test_highest_frequency(text, frequency):
    """Ensure the highest frequency is returned."""
    assert ANALYZER.calculate_highest_frequency(text) == frequency


@pytest.mark.parametrize("text,word,frequency", [
    ("", "foo", 0),
    ("foo", "", 0),
    ("foo", " ", 0),
    ("foo", "___", 0),
    ("123", "123", 0),
    ("foo", "foo", 1),
    ("foo", " foo", 0),
    ("foo", "*foo*", 0),
    ("foo foo", "*foo*", 0),
    ("foo-bar", "foo-bar", 0),
    ("foo foo", "foo", 2),
    ("foo bar", "bar", 1),
    ("foo foo bar", "foo", 2),
    ("foo foo bar", "bar", 1),
    ("foo foo bar bar bar", "foo", 2),
    ("foo foo bar bar bar", "bar", 3),
    ("foo FOO Foo fOo foO FOo fOO FoO bar", "foo", 8),
    ("foo FOO Foo fOo foO FOo fOO FoO bar-bar", "bar", 2),
])
def test_finding_the_correct_frequency_for_a_word_in_text(text, word, frequency):
    assert ANALYZER.calculate_frequency_for_word(text, word) == frequency


@pytest.mark.parametrize("text,n,expected", [
    ("foo", 0, []),
    ("", 0, []),
    ("", 1, []),
    ("foo", 1, [("foo", 1)]),
    ("foo bar bar aa aa aa", 1, [("aa", 3)]),
    ("foo bar bar aa aa aa", 2, [("aa", 3), ("bar", 2)]),
    ("foo bar bar aa aa aa", 3, [("aa", 3), ("bar", 2) , ("foo", 1)]),
    ("ac aa ab", 3, [("aa", 1), ("ab", 1), ("ac", 1)]),
    ("ab aa ac dd", 4, [("aa", 1), ("ab", 1), ("ac", 1), ("dd", 1)]),
    ("The sun shines over the lake", 3, [("the", 2), ("lake", 1), ("over", 1)])
])
def test_finding_most_frequenct_n_words(text, n, expected):
    assert ANALYZER.calculate_most_frequent_n_words(text, n) == expected
