# How to run tests

While being in the directory of the project run:

`$ pip install -r requirements.txt`

`$ pytest`


#### Notes
 - The WordFrequency interface was not used as I do not find it necessary at this point.
